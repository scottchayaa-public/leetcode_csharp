﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using leetcode_csharp.Leetcode;
using System;
using System.Collections.Generic;
using System.Text;

namespace leetcode_csharp.Leetcode.Tests
{
    [TestClass()]
    public class P020Tests
    {
        P020 target = new P020();

        [TestMethod()]
        public void MySolutionTest()
        {
            // arrange
            string param1 = "()";
            bool expected = true;

            // act
            bool actual = target.MySolution(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }
        
        [TestMethod()]
        public void OtherSolutionTest()
        {

        }
    }
}