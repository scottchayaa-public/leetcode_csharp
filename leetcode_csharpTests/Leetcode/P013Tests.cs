﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using leetcode_csharp.Leetcode;
using System;
using System.Collections.Generic;
using System.Text;

namespace leetcode_csharp.Leetcode.Tests
{
    [TestClass()]
    public class P013Tests
    {
        P013 target = new P013();

        [TestMethod()]
        public void MySolutionTest()
        {
            // arrange
            string param1 = "IIX";
            int expected = 10;

            // act
            int actual;
            actual = target.MySolution(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void OtherSolutionTest()
        {
            // arrange
            string param1 = "IIX";
            int expected = 10;

            // act
            int actual;
            actual = target.OtherSolution(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }
    }
}