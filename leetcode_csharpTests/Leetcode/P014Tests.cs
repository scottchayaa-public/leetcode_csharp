﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using leetcode_csharp.Leetcode;
using System;
using System.Collections.Generic;
using System.Text;

namespace leetcode_csharp.Leetcode.Tests
{
    [TestClass()]
    public class P014Tests
    {
        P014 target = new P014();

        [TestMethod()]
        public void MySolutionTest()
        {
            // arrange
            string[] param1 = new string[] { "flower", "flow", "flight" };
            string expected = "fl";

            // act
            string actual;
            actual = target.MySolution(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void MySolutionTest2()
        {
            // arrange
            string[] param1 = new string[] { "dog", "racecar", "car" };
            string expected = "";

            // act
            string actual;
            actual = target.MySolution2(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void MySolutionTest3()
        {
            // arrange
            string[] param1 = new string[] { "ab", "a" };
            string expected = "a";

            // act
            string actual;
            actual = target.MySolution2(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void OtherSolutionTest()
        {

        }
    }
}