﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using leetcode_csharp.Leetcode;
using System;
using System.Collections.Generic;
using System.Text;

namespace leetcode_csharp.Leetcode.Tests
{
    [TestClass()]
    public class P009Tests
    {
        P009 target = new P009();

        [TestMethod()]
        public void Solution1Test()
        {
            // arrange
            int param1 = 121;
            bool expected = true;

            // act
            bool actual;
            actual = target.Solution1(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Solution1Test2()
        {
            // arrange
            int param1 = 10;
            bool expected = false;

            // act
            bool actual;
            actual = target.Solution1(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Solution1Test3()
        {
            // arrange
            int param1 = 1001;
            bool expected = true;

            // act
            bool actual;
            actual = target.Solution1(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Solution2Test3()
        {
            // arrange
            int param1 = 1001;
            bool expected = true;

            // act
            bool actual;
            actual = target.Solution2(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }
    }
}