﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using leetcode_csharp.Leetcode;
using System;
using System.Collections.Generic;
using System.Text;

namespace leetcode_csharp.Leetcode.Tests
{
    [TestClass()]
    public class P005Tests
    {
        [TestMethod()]
        public void LongestPalindromeTest()
        {
            // arrange
            P005 target = new P005();
            string param1 = "babad";
            string expected = "bab";

            // act
            string actual;
            actual = target.LongestPalindrome(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void LongestPalindromeTest2()
        {
            // arrange
            P005 target = new P005();
            string param1 = "cbbd";
            string expected = "bb";

            // act
            string actual;
            actual = target.LongestPalindrome(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void LongestPalindromeTest3()
        {
            // arrange
            P005 target = new P005();
            string param1 = "a";
            string expected = "a";

            // act
            string actual;
            actual = target.LongestPalindrome(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void LongestPalindromeTest4()
        {
            // arrange
            P005 target = new P005();
            string param1 = "aacabdkacaa";
            string expected = "aca";

            // act
            string actual;
            actual = target.LongestPalindrome(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void LongestPalindromeTest5()
        {
            // arrange
            P005 target = new P005();
            string param1 = "bb";
            string expected = "bb";

            // act
            string actual;
            actual = target.LongestPalindrome(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void LongestPalindromeTest6()
        {
            // arrange
            P005 target = new P005();
            string param1 = "yzwhuvljgkbxonhkpnxldwkaiboqoflbotqamsxyglfqniflcrtsxbsxlwmxowwnnxychyrjedlijejqzsgwakzohghpxgamecmhcalfoyjtutxeciwqupwlxrgdcpfvybskrytvmwkvnbdjitmohjavhmjobudvbsnkvszyrahpanocltwzeubgxkkthxhjgvcvygfkjctkubtjdocncmjzmxujetybdwmqutvrrulhlsbcbripctbkacwoutkrqsohiihiegqqlasykkgjjskgphieofsjlkkmvwcltgjqbpakercxypfcqqsmkowmgjglbzbidapmqoitpzwhupliynjynsdfncaosrfegquetyfhfqohxytqhjxxpskpwxegmnnppnnmgexwpkspxxjhqtyxhoqfhfyteuqgefrsoacnfdsnyjnyilpuhwzptioqmpadibzblgjgmwokmsqqcfpyxcrekapbqjgtlcwvmkkljsfoeihpgksjjgkkysalqqgeihiihosqrktuowcakbtcpirbcbslhlurrvtuqmwdbytejuxmzjmcncodjtbuktcjkfgyvcvgjhxhtkkxgbuezwtlconapharyzsvknsbvdubojmhvajhomtijdbnvkwmvtyrksbyvfpcdgrxlwpuqwicextutjyoflachmcemagxphghozkawgszqjejildejryhcyxnnwwoxmwlxsbxstrclfinqflgyxsmaqtoblfoqobiakwdlxnpkhnoxbkgjlvuhwzy";
            string expected = "yzwhuvljgkbxonhkpnxldwkaiboqoflbotqamsxyglfqniflcrtsxbsxlwmxowwnnxychyrjedlijejqzsgwakzohghpxgamecmhcalfoyjtutxeciwqupwlxrgdcpfvybskrytvmwkvnbdjitmohjavhmjobudvbsnkvszyrahpanocltwzeubgxkkthxhjgvcvygfkjctkubtjdocncmjzmxujetybdwmqutvrrulhlsbcbripctbkacwoutkrqsohiihiegqqlasykkgjjskgphieofsjlkkmvwcltgjqbpakercxypfcqqsmkowmgjglbzbidapmqoitpzwhupliynjynsdfncaosrfegquetyfhfqohxytqhjxxpskpwxegmnnppnnmgexwpkspxxjhqtyxhoqfhfyteuqgefrsoacnfdsnyjnyilpuhwzptioqmpadibzblgjgmwokmsqqcfpyxcrekapbqjgtlcwvmkkljsfoeihpgksjjgkkysalqqgeihiihosqrktuowcakbtcpirbcbslhlurrvtuqmwdbytejuxmzjmcncodjtbuktcjkfgyvcvgjhxhtkkxgbuezwtlconapharyzsvknsbvdubojmhvajhomtijdbnvkwmvtyrksbyvfpcdgrxlwpuqwicextutjyoflachmcemagxphghozkawgszqjejildejryhcyxnnwwoxmwlxsbxstrclfinqflgyxsmaqtoblfoqobiakwdlxnpkhnoxbkgjlvuhwzy";

            // act
            string actual;
            actual = target.LongestPalindrome(param1);

            // assert
            Assert.AreEqual(expected, actual);
        }
    }
}