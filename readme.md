# Leetcode csharp

## 實作項目


## 單元測試

安裝 coverlet Package
```sh
cd ./leetcode_csharpTests
dotnet add package coverlet.msbuild
```

測試分析並產出 html 報告
```sh
dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput=../coverage/
reportgenerator -reports:./coverage/coverage.opencover.xml -reporttypes:Html -targetdir:./coverage
```