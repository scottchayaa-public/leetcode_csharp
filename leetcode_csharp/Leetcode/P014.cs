﻿
using System;
using System.Collections.Generic;

namespace leetcode_csharp.Leetcode
{
    /// <summary>
    /// 14. Longest Common Prefix
    /// https://leetcode.com/problems/longest-common-prefix/
    /// </summary>
    public class P014
    {
        public string LongestCommonPrefix(string[] strs)
        {
            return MySolution2(strs);
        }

        public string MySolution(string[] strs)
        {
            string result = "";
            int subStringLength = 1;
            bool IsInterupt = false;

            try
            {
                while (!IsInterupt)
                {
                    string comparingStr = "";
                    for (int i = 0; i < strs.Length; i++)
                    {
                        var str = strs[i].Substring(0, subStringLength);
                        if (comparingStr != "" && comparingStr != str)
                        {
                            IsInterupt = true;
                            break;
                        }
                        comparingStr = str;
                    }

                    if (!IsInterupt)
                    {
                        result = comparingStr;
                        subStringLength++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return result;
        }

        public string MySolution2(string[] strs)
        {
            if (strs.Length == 0) return "";
            if (strs.Length == 1) return strs[0];

            var baseStr = strs[0];

            var prefixStr = "";

            for (int i = 0; i < baseStr.Length; i++)
            {
                foreach (var str in strs)
                {
                    if (i > str.Length - 1)
                    {
                        return prefixStr;
                    }

                    if (baseStr[i] != str[i])
                    {
                        return prefixStr;
                    }
                }

                prefixStr += strs[0][i];
            }

            return prefixStr;
        }
    }
}
