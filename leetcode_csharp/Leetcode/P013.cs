﻿
using System;
using System.Collections.Generic;

namespace leetcode_csharp.Leetcode
{
    /// <summary>
    /// 13. Roman to Integer
    /// https://leetcode.com/problems/roman-to-integer/
    /// </summary>
    public class P013
    {
        public int RomanToInt(string s)
        {
            //return MySolution(s);
            return OtherSolution(s);
        }

        public int MySolution(string s)
        {
            var RomanInt = new Dictionary<char, int>()
            {
                { 'I', 1 },
                { 'V', 5 },
                { 'X', 10 },
                { 'L', 50 },
                { 'C', 100 },
                { 'D', 500 },
                { 'M', 1000 }
            };

            var result = 0;
            var tmp = RomanInt[s[0]]; // init first value

            for (int i = 1; i < s.Length; i++)
            {
                if (RomanInt[s[i]] > tmp)
                {
                    tmp = RomanInt[s[i]] - tmp;
                }
                else
                {
                    result += tmp;
                    tmp = RomanInt[s[i]];
                }
            }

            result += tmp;

            return result;
        }

        public int OtherSolution(string s)
        {
            int decimalResult = 0;
            int lastNumber = 0;
            int length = s.Length;

            for (int index = 0; index < length; index++)
            {
                char letter = s[index];
                int currentNum = 0;

                //switch is faster than if-else statements
                switch (letter)
                {
                    case 'I':
                        currentNum = 1; break;
                    case 'V':
                        currentNum = 5; break;
                    case 'X':
                        currentNum = 10; break;
                    case 'L':
                        currentNum = 50; break;
                    case 'C':
                        currentNum = 100; break;
                    case 'D':
                        currentNum = 500; break;
                    case 'M':
                        currentNum = 1000; break;
                }

                //The previous number (number of the previous iteration, if any) was already added
                //but if that number is actually smaller than the new (current) one,
                //then the previous had to be subtracted, not added. 
                //So it is necessary to subtract it (the previous one) two times (2* lastNumber):
                //Once to compensate adding it wrong the first time, and a second time to dothe right operation.
                if (currentNum > lastNumber)
                    decimalResult -= (2 * lastNumber);

                //Each new digit is added without any further consideration
                decimalResult += currentNum;

                //The loop is done, but we keep this new number in memory for the previous comparation
                lastNumber = currentNum;
            }
            return decimalResult;
        }
    }
}
