﻿
using System;

namespace leetcode_csharp.Leetcode
{
    /// <summary>
    /// 9. Palindrome Number
    /// https://leetcode.com/problems/palindrome-number/
    /// </summary>
    public class P009
    {
        public bool IsPalindrome(int x)
        {
            //return Solution1(x);
            return Solution2(x);
        }

        public bool Solution2(int x)
        {
            if (x < 0) return false;

            var reverse = 0;
            var y = x;
            while (y > 0) {
                reverse = reverse * 10 + y % 10;
                y = y / 10;
            }

            return reverse == x;
        }

        public bool Solution1(int x)
        {
            if (x < 0) return false;

            var dl = getDigitLength(x);

            for (int i = 0; i < dl / 2; i++)
            {
                var L = (x / (int)Math.Pow(10, dl - i - 1)) % 10;
                var R = (x % (int)Math.Pow(10, i + 1)) / (int)Math.Pow(10, i);
                if (L != R)
                {
                    return false;
                }
            }

            return true;
        }

        public int getDigitLength(int x)
        {
            int n = 0;
            while (Math.Pow(10, n) <= x)
            {
                n++;
            }
            return n;
        }
    }
}
