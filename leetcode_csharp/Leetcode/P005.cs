﻿
using System;

namespace leetcode_csharp.Leetcode
{
    /// <summary>
    /// 5. Longest Palindromic Substring
    /// https://leetcode.com/problems/longest-palindromic-substring/
    /// </summary>
    public class P005
    {
        public string LongestPalindrome(string s)
        {
            int maxLength = 1;
            var maxLengthStr = s[0].ToString();

            for (int i = 0; i < s.Length; i++)
            {
                //Odd Palindrom
                int L = i; int R = i;
                while (L >= 0 && R < s.Length && s[L] == s[R])
                {
                    if (R - L + 1 > maxLength)
                    {
                        maxLength = R - L + 1;
                        maxLengthStr = s.Substring(L, R - L + 1);
                    }
                    L--;
                    R++;
                }

                //Even Palindrom
                L = i; R = i + 1;
                while (L >= 0 && R < s.Length && s[L] == s[R])
                {
                    if (R - L + 1 > maxLength)
                    {
                        maxLength = R - L + 1;
                        maxLengthStr = s.Substring(L, R - L + 1);
                    }
                    L--;
                    R++;
                }

            }
            return maxLengthStr;
        }
    }
}
